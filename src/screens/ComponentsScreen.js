import React from "react";
import { Text, StyleSheet, View } from "react-native";

const name = "Anna";

const ComponentsScreen = () => {
  return (
    <View>
      <Text style={styles.textStyle}>Getting started with React Native</Text>
      <Text style={styles.subHeading}>My name is {name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 45,
  },
  subHeading: {
    fontSize: 20,
  },
});

export default ComponentsScreen;
