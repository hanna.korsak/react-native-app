import React from "react";
import { Text, StyleSheet, View, FlatList } from "react-native";

const friends = [
  { name: "Anna", age: 32 },
  { name: "Alex", age: 33 },
  { name: "Olga", age: 23 },
  { name: "Andy", age: 25 },
];
const ListScreen = () => {
  return (
    <FlatList
      keyExtractor={(friend) => friend.name}
      data={friends}
      renderItem={({ item }) => {
        return (
          <Text style={styles.textStyle}>
            {item.name} - Age {item.age}
          </Text>
        );
      }}
    ></FlatList>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 18,
    marginVertical: 120,
  },
});

export default ListScreen;
