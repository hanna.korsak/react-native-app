import React from "react";
import { Text, StyleSheet, Button, View } from "react-native";

const BoxScreen = ({}) => {
  return (
    <View>
      <View style={styles.headerStyle}>
        <Text style={styles.textStyle}>App</Text>
      </View>
      <View style={styles.viewBoxesStyle}>
        <View style={styles.boxOneStyle}></View>
        <View style={styles.boxTwoStyle}></View>
        <View style={styles.boxThreeStyle}></View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  ViewStyle: {},
  headerStyle: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "steelblue",
    alignItems: "stretch",
    height: 40,
  },
  textStyle: {
    alignSelf: "center",
  },
  viewBoxesStyle: {
    backgroundColor: "aliceblue",
    flexDirection: "row",
    justifyContent: "space-between",
    height: 200,
  },

  boxOneStyle: {
    backgroundColor: "powderblue",
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: "steelblue",
  },
  boxTwoStyle: {
    backgroundColor: "skyblue",
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: "steelblue",
    alignSelf: "flex-end",
  },
  boxThreeStyle: {
    backgroundColor: "steelblue",
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: "steelblue",
  },
});

export default BoxScreen;
